Tiles
===
A javascript concentration game written without jQuery.

Click go. You have 5 seconds to memorize the tiles. Then, select the right tiles. No wrong answers.

You can dynamically change the grid size to add more tiles by using rows and columns options at the bottom.

Using with Grunt
---
With grunt and grunt cli installed.

     npm install

Run tests
---
Spin up a web server on port 8080

     http-server -c-1
     grunt

Build a production version in build/
---

     grunt build

Build dev and watch for sass/js changes
---

     grunt dev


[live demo](http://briankotch.com/tiles)

Special thanks to:
---

[bootstrap 3](http://getbootstrap.com) For the excellent starting point.

[sass](http://sass-lang.com) For making css manageable.

[majodev](https://github.com/majodev/requirejs-nodewebkit-template-project) For a bitching build script that takes all the pain out of grunt, requirejs and node
