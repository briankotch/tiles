/*global define, describe, it */
define(["tiles"],
       function(Tiles) {
           describe("Tiles", function() {
               it("Tiles initialized", function() {
                   Tiles.init(5, 5);
                   Tiles.columns.should.equal(5);
                   Tiles.tiles.should.equal(25);


               });
               it("Tiles resized", function() {
                   Tiles.init(7, 7);
                   Tiles.tiles.should.equal(49);
               });
               it("Tiles winnable", function() {
                   var i, user_moves = [];
                   Tiles.init(5,5);
                   for(i = 1; i <= Tiles.answer_cells.length; i = i + 1) {
                       if(Tiles.answer_cells[i]) {
                           user_moves.push(i);
                       }
                   }
                   Tiles.is_won(user_moves).should.equal(true);
                   user_moves.length.should.equal(9);
               });
               it("Tiles losable", function() {
                   var i, user_moves = [];
                   Tiles.init(5, 5);
                   for(i = 1; i <= Tiles.tiles; i = i + 1) {
                       //we're not in the list
                       if (!Tiles.answer_cells[i]) {
                           user_moves.push(i);
                       }
                   }
                   Tiles.is_won(user_moves).should.equal(false);
               });
           });
       });
