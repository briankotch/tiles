/*global define, describe, it */
define(["appConfig", "underscore"],
       function(appConfig, underscore) {
           describe("appConfig", function() {
               it("has field loglevel", function() {
                   underscore.isUndefined(appConfig.logLevel).should.equal(false);
               });
           });
       });
