/*global define, document */
define(["log", "messages", "Poll", "appConfig", "Ui"], function (log, messages, Poll, appConfig, Ui) {
    "use strict";

    var debugElement = document.getElementById("debugText"),
        runningForSeconds = 0;

    function pollTimerTest() {
        runningForSeconds += 1;
        // output it directly to the node.
        debugElement.innerHTML = messages.running_for({
            seconds: runningForSeconds
        });
    }

    (function preloading() {
        // sets the loglevel
        log.setLevel(appConfig.logLevel);

        // debug test with log.
        log.debug(messages.log_level({'log_level': appConfig.logLevel}));

        // test the shimmed lib Poll
        Poll.start({
            name: "timerTest",
            interval: 1000,
            action: pollTimerTest
        });

    }());

    (function main() {
        Ui.init();
    }());

});
