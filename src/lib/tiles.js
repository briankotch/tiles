/*global define */
define([], function () {
    "use strict";
    function Tiles () {
        var instance = this,
        //rows * columns = number of tiles
        //(number of tiles * percentaged needed) / 100 rounded down = number of tiles needed to win
        rows = 5,
        moves_required = 8,
        percentage_required = 35;

        instance.columns = 5;
        instance.tiles = 25;
        //randomly generated correct tiles
        instance.answer_cells = [];

        function calculate_moves_required() {
            moves_required = Math.ceil((instance.tiles * percentage_required) / 100);
            return moves_required;
        }

        function generate_random_answers() {
            var  i = 0, answer;
            instance.answer_cells = [];
            while (i < moves_required) {
                answer = Math.floor(Math.random() * instance.tiles) + 1;
                if (!instance.answer_cells[answer]) {
                    i = i + 1;
                    instance.answer_cells[answer] = true;
                }
            }
        }

        instance.init = function (x, y) {
            rows = x;
            instance.columns = y;
            moves_required = Math.floor((x * y) /100);
            instance.tiles = x * y;
            instance.answer_cells = [];
            calculate_moves_required();
            generate_random_answers(rows, instance.columns);
        };

        instance.is_won = function(user_moves) {
            var i = 0;
            if (user_moves.length < moves_required) {
                return false;
            }
            for(i = 0; i < user_moves.length; i = i + 1) {
                if (instance.answer_cells[user_moves[i]] === undefined) {
                    return false;
                }
            }
            return true;
        };


    }
    return new Tiles();
});
