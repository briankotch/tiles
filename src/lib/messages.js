/*global define */
define(["underscore"], function(Underscore) {
    "use strict";
    //Since javascript lacks a proper sprintf and since string concat is of the devil
    //I am storing all my javascript messages that need values in one object and using
    //lodash's templates.
    function Messages() {
        var instance = this;
        instance.log_level = Underscore.template('Log level is <%= log_level %>');
        instance.running_for = Underscore.template('running since <%= seconds %> seconds');
        instance.binding_to = Underscore.template('binding to <%= element %>');
        instance.generating_board = Underscore.template('generating board with <%= columns %> columns and <%= rows %> rows');
    }
    return new Messages();
});
