/*global define, document, window */
define(["log", "tiles", "messages"], function(Log, Tiles, Messages) {
    "use strict";
    function Ui () {
        //Private functions

        //John Resig's simple code to deal with IE's stupidity. Until we stop caring about ie eight, we need to do this.
        //It has the added advantaged of handling event bumbling
        //I could move this stuff to the document, but there is little need when this Ui module controls it all
        function addEvent( obj, type, fn ) {
            if ( obj.attachEvent ) {
                obj['e'+type+fn] = fn;
                obj[type+fn] = function(){obj['e'+type+fn]( window.event );};
                obj.attachEvent( 'on'+type, obj[type+fn] );
            } else {
                obj.addEventListener( type, fn, false );
            }
        }

        function removeEvent( obj, type, fn ) {
            if ( obj.detachEvent ) {
                obj.detachEvent( 'on'+type, obj[type+fn] );
                obj[type+fn] = null;
            } else {
                obj.removeEventListener( type, fn, false );
            }
        }

        function bind_tiles(tile_elements, callback) {
            var i;
            for(i = 0; i < tile_elements.length; i++) {
                //no double binding memory leaks, please
                if(tile_elements[i].onclick !== "function") {
                    addEvent(tile_elements[i], "click", callback);
                }
            }
        }

        //Private variables
        //Take that, contextual this
        var instance = this,
        //minimum number of columns/rows
        columns_floor = 5,
        //maximum number of columns/rows
        columns_ceiling = 7,
        ///How long the user has to memorize the proper tiles
        count_down_interval = 5,
        //Flag for accepting tile clicks
        user_can_move = false,
        //list of moves the user has made
        user_moves = [],
        //static elements that we use often
        count_down_element = document.getElementById("countdown"),
        count_down_message_element = document.getElementById("countdown_message"),
        count_down_title_element = document.getElementById("countdown_title"),
        count_down_button_element = document.getElementById("countdown_button");

        //Module methods
        //Call this to wire up the ui
        //It assumes the HTML5 technique of including the javascript at the bottom so the dom is ready
        instance.init = function() {
            var go_buttons, i;
            Log.debug("UI initializing.");
            go_buttons = document.querySelectorAll(".button_go");
            //Downside of not using jquery is no magical iteration over the node list.
            for(i = 0; i < go_buttons.length; i = i + 1) {
                Log.debug(Messages.binding_to({element: go_buttons[i]}));
                //I hate anonymous functions too, unless I am doing something functional
                removeEvent(go_buttons[i], "click", instance.start_game);
                addEvent(go_buttons[i], "click", instance.start_game);
            }
        };

        //Reset the ui
        instance.reset = function() {
            user_moves = [];
            count_down_element.classList.remove("move_to_top");
            count_down_title_element.classList.remove("invisibo");
            count_down_message_element.classList.remove("invisibo");
            count_down_button_element.classList.remove("invisibo");

        };

        //Update the number of rows and columns from the ui
        instance.update_tiles = function() {
            var rows_element, columns_element, rows, columns;
            rows_element = document.getElementById("rows");
            columns_element = document.getElementById("columns");
            rows = rows_element.options[rows_element.selectedIndex].text;
            columns = columns_element.options[columns_element.selectedIndex].text;
            Log.debug(Messages.generating_board({ 'rows': rows, 'columns': columns }));

            //Update the board in the tiles controller
            Tiles.init(rows, columns);
            //Then populate the ui
            instance.draw_board();
            Log.debug(Tiles.answer_cells);
        };

        //Redraw the board based on the configuration of the Tiles
        instance.draw_board = function() {
            var tile_elements, board_element, tiles_count, i;
            tile_elements  = document.querySelectorAll('.tile');
            board_element = document.getElementById('board');
            //Line breaks for the tile rows
            for(i = columns_floor; i < columns_ceiling; i = i + 1) {
                board_element.classList.remove("row" + i);
            }
            board_element.classList.add("row" + Tiles.columns);

            //force redraw for borders
            board_element.classList.remove("clearfix");

            tiles_count = tile_elements.length;
            //If we're shrinking the board
            if (tiles_count > Tiles.tiles) {
                Log.debug("Removing tiles");
                //pop off elements from the end of the ties and delete them from the board
                for(i = tiles_count; i > Tiles.tiles; i = i -1) {
                    board_element.removeChild(tile_elements[i]);
                }
            }
            //if we're growing the board
            if(tile_elements.length < Tiles.tiles) {
                Log.debug("Adding tiles");
                var tile_element;
                //Starting at n + 1 tiles.
                for(i = tiles_count + 1; i <= Tiles.tiles; i = i + 1) {
                    tile_element = document.createElement("div");
                    tile_element.textContent = i;
                    tile_element.setAttribute("class", "tile");
                    Log.debug(tile_element);
                    board_element.appendChild(tile_element);
                }
            }
            board_element.classList.add("clearfix");
            bind_tiles(tile_elements, instance.select_tile);
        };

        instance.select_tile = function(event) {
            Log.debug("clicked.");
            Log.debug(Tiles.answer_cells);
            if (user_can_move) {
                var tile_value = event.target.textContent;
                Log.debug(tile_value);
                Log.debug(Tiles.answer_cells);
                if (Tiles.answer_cells[tile_value]) {
                    user_moves.push(tile_value);
                    event.target.classList.add("winner");
                    Log.debug("Correct!");
                } else {
                    user_can_move = false;
                    count_down_title_element.textContent = "You lose!";
                    count_down_message_element.textContent = "Boohoo";
                    instance.reset();
                    Log.debug("Incorrect!");
                }
                if (Tiles.is_won(user_moves)) {
                    instance.reset();
                    count_down_title_element.textContent = "Congratulations";
                    count_down_message_element.textContent = "You win!";
                }
            }
        };

        instance.start_game = function(event) {
            //One of the buttons is a form, because I like semantic markup
            event.preventDefault();
            instance.reset();
            instance.update_tiles();
            //Cute names for classes to work around Bootstraps brilliant decision
            //To include a .hidden that uses display:none which is an accessibility no no
            count_down_element.classList.toggle("move_to_top");
            count_down_title_element.classList.toggle("invisibo");
            count_down_button_element.classList.toggle("invisibo");
            instance.draw_board();
            instance.toggle_winning_tiles(true);
            count_down_interval = 5;
            instance.count_down();
            Log.debug("Go button clicked.");
        };

        instance.toggle_winning_tiles = function(show) {
            var tile_elements, i;
            tile_elements = document.querySelectorAll(".tile");
            for (i = 0; i < tile_elements.length; i = i + 1) {
                tile_elements[i].classList.remove("winner");
                if(show) {
                    Log.debug("showing " + i);
                    if (Tiles.answer_cells[i]) {
                        //adjusting for 0 index
                        tile_elements[i - 1].classList.add("winner");
                    }
                }
            }
        };

        //If I wanted something more robust, I would slave my timers into another module that
        //raises events froma web worker. I need a paycheck to get that low level
        instance.count_down = function() {
            instance.running = true;
            if(count_down_interval >= 1) {
                count_down_message_element.textContent = count_down_interval;
                count_down_interval = count_down_interval - 1;
                setTimeout(instance.count_down, 1000);
            } else {
                instance.toggle_winning_tiles(false);
                count_down_message_element.textContent = "Go!";
                user_can_move = true;
                Log.debug("Start the game!");
            }
        };
    }
    return new Ui();
});
